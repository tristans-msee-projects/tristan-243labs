//Tristan French
//Samir Mohammed

#include "unity.h"
#include "can_lab.h"
#include "Mockcan.h"

can_lab_S can_inst;
can_lab_S* can_inst_ptr = &can_inst;
can_msg_t tx_can_message;
can_msg_t* tx_can_msg_ptr = &tx_can_message;
can_msg_t rx_can_message;
can_msg_t* rx_can_msg_ptr = &rx_can_message;
can_msg_t rx_received_can_message;
can_msg_t* rx_received_can_message_ptr = &rx_received_can_message;

void setUp(void) {
    can_inst.can_lab_can = can1;
    can_inst.can_lab_baudrate_kbps = 100;
    can_inst.can_lab_rxq_size = 10;
    can_inst.can_lab_txq_size = 10;
    can_inst.can_lab_bus_off_cb = NULL;
    can_inst.can_lab_data_ovr_cb = NULL;
    can_inst.can_lab_timeout_ms = 0;

}

void tearDown(void) {
}

void test_can_lab_init(void){
    CAN_init_ExpectAndReturn(can_inst.can_lab_can, can_inst.can_lab_baudrate_kbps,
            can_inst.can_lab_rxq_size, can_inst.can_lab_txq_size,
            can_inst.can_lab_bus_off_cb, can_inst.can_lab_data_ovr_cb, true);
    TEST_ASSERT_TRUE(can_lab_init(can_inst_ptr));
}

void test_can_lab_tx(void){
    tx_can_message.frame_fields.data_len = 1;
    tx_can_message.frame_fields.is_rtr = false;
    tx_can_message.frame_fields.is_29bit = false;
    tx_can_message.msg_id = 182;
    tx_can_message.data.bytes[0] = 'a';

    CAN_tx_ExpectAndReturn(can_inst.can_lab_can, tx_can_msg_ptr, can_inst.can_lab_timeout_ms, true);
    TEST_ASSERT_TRUE(can_lab_tx(can_inst_ptr, tx_can_msg_ptr));

}

void test_can_lab_rx(void)
{
    rx_can_message.frame_fields.data_len = 1;
    rx_can_message.frame_fields.is_rtr = false;
    rx_can_message.frame_fields.is_29bit = false;
    rx_can_message.msg_id = 182;
    rx_can_message.data.bytes[0] = 'a';


    CAN_rx_ExpectAndReturn(can_inst.can_lab_can, rx_received_can_message_ptr, can_inst.can_lab_timeout_ms, true);
    CAN_rx_ReturnThruPtr_msg(&rx_can_message);
    TEST_ASSERT_TRUE(can_lab_rx(can_inst_ptr, rx_received_can_message_ptr));

    TEST_ASSERT_EQUAL(rx_can_message.frame, rx_received_can_message.frame);
    TEST_ASSERT_EQUAL(rx_can_message.msg_id, rx_received_can_message.msg_id);
    TEST_ASSERT_EQUAL(rx_can_message.data.qword, rx_received_can_message.data.qword);

}

void test_can_lab_reset(void)
{
    CAN_is_bus_off_ExpectAndReturn(can_inst.can_lab_can, true);
    CAN_reset_bus_Expect(can_inst.can_lab_can);
    TEST_ASSERT_TRUE(can_lab_reset(can_inst_ptr));

    CAN_is_bus_off_ExpectAndReturn(can_inst.can_lab_can, false);
    TEST_ASSERT_FALSE(can_lab_reset(can_inst_ptr));
}

