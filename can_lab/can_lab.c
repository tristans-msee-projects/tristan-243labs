/*
 * can_lab.c
 *
 *  Created on: Mar 3, 2019
 *      Author: reldn_000
 */
//Tristan French
//Samir Mohammed

#include "can_lab.h"
#include "stdbool.h"

bool can_lab_init(can_lab_S* params){
    bool init_successfull = CAN_init(params->can_lab_can, params->can_lab_baudrate_kbps,
            params->can_lab_rxq_size, params->can_lab_txq_size,
            params->can_lab_bus_off_cb, params->can_lab_data_ovr_cb);

    return init_successfull;
}

bool can_lab_tx(can_lab_S* params, can_msg_t* msg_ptr){
    bool tx_successfull = CAN_tx(params->can_lab_can, msg_ptr, params->can_lab_timeout_ms);

    return tx_successfull;
}

bool can_lab_rx(can_lab_S* params, can_msg_t* msg_ptr){
    bool rx_successfull = CAN_rx(params->can_lab_can, msg_ptr, params->can_lab_timeout_ms);

    return rx_successfull;
}
bool can_lab_reset(can_lab_S* params){
    bool reset_occurred = false;
    if ((CAN_is_bus_off(params->can_lab_can)))
    {
        CAN_reset_bus(params->can_lab_can);
        reset_occurred = true;
    }
    return  reset_occurred;
}


