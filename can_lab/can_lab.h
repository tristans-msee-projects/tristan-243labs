/*
 * can_lab.h
 *
 *  Created on: Mar 3, 2019
 *      Author: reldn_000
 */
//Tristan French
//Samir Mohammed

#ifndef CAN_LAB_H_
#define CAN_LAB_H_

#include "can.h"
#include <stdint.h>
#include <stdbool.h>

typedef struct{
    can_t can_lab_can;
    uint32_t can_lab_baudrate_kbps;
    uint16_t can_lab_rxq_size;
    uint16_t can_lab_txq_size;
    can_void_func_t can_lab_bus_off_cb;
    can_void_func_t can_lab_data_ovr_cb;
    uint32_t can_lab_timeout_ms;
}can_lab_S;

bool can_lab_init(can_lab_S* params);
bool can_lab_tx(can_lab_S* params, can_msg_t* msg_ptr);
bool can_lab_rx(can_lab_S* params, can_msg_t* msg_ptr);
bool can_lab_reset(can_lab_S* params);



#endif /* CAN_LAB_H_ */
