/*
 * c_periodic_callbacks.c
 *
 *  Created on: Feb 16, 2019
 *      Author: reldn_000
 */

#include "c_periodic_callbacks.h"
#include <stdio.h>
#include <stdbool.h>
#include "uart_wrapper.h"
#include "LED_Display_wrapper.h"
#include "can.h"
#include "c_gpio.h"
#include "generated_can.h"

can_msg_t tx_can_message;
can_msg_t* tx_can_msg_ptr = &tx_can_message;
can_msg_t rx_can_message;
can_msg_t* rx_can_msg_ptr = &rx_can_message;


bool c_period_init(void)
{
    bool init_success = LED_wrap_init();

    CAN_init(can1, 100, 10, 10, NULL, NULL);

    CAN_reset_bus(can1);
    CAN_bypass_filter_accept_all_msgs();

    tx_can_message.frame_fields.data_len = 8;
    tx_can_message.frame_fields.is_rtr = false;
    tx_can_message.frame_fields.is_29bit = false;
    tx_can_message.msg_id = 10;
    tx_can_message.data.bytes[0] = 'A';

    return init_success;
}

bool c_period_reg_tlm(void)
{
    return true;
}

void c_period_1Hz(uint32_t count)
{
    if ((CAN_is_bus_off(can1)))
    {
        CAN_reset_bus(can1);
    }

    (void) count;
}
void c_period_10Hz(uint32_t count)
{
//    if (read_a_switch(1)){
//        tx_can_message.data.bytes[0] = 1;
//    } else if (read_a_switch(2)) {
//        tx_can_message.data.bytes[0] = 2;
//    } else if (read_a_switch(3)) {
//        tx_can_message.data.bytes[0] = 3;
//    } else if (read_a_switch(4)) {
//        tx_can_message.data.bytes[0] = 4;
//    } else{
//        tx_can_message.data.bytes[0] = 0;
//    }
//
//    CAN_tx(can1, tx_can_msg_ptr, 0);
}

void c_period_100Hz(uint32_t count)
{
   
    if (CAN_rx(can1, rx_can_msg_ptr, 0))
    {
        LED_wrap_setNumber((rx_can_message.data.qword));
    }

}
void c_period_1000Hz(uint32_t count)
{
    (void) count;
}
