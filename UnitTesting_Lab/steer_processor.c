#include "steer_processor.h"
#include <stdint.h>
uint32_t Threshold = 50;
void steer_processor(uint32_t left_sensor_cm, uint32_t right_sensor_cm){
    if (left_sensor_cm < Threshold){
        if (right_sensor_cm <= left_sensor_cm){
            steer_left();
        }
        else
        {
            steer_right();
        }
    }
    else if (right_sensor_cm < Threshold){
        steer_left();
    }
    else{}
}
