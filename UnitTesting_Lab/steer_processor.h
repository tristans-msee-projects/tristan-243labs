#pragma once
#include "steering.h"
#include <stdint.h>

/**
 * Assume that a threshold value is 50cm
 * Objective is to steer left or right based on the left and right sensor values
 * Example: If left sensor is 10cm, and right is 70cm, then we should call steer_right()
 */
void steer_processor(uint32_t left_sensor_cm, uint32_t right_sensor_cm);
