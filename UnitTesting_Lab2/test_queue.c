#include "unity.h"
#include "queue.h"

queue_S queue_inst;
queue_S* queue_inst_ptr = &queue_inst;
uint8_t pop_value;
uint8_t* pop_ptr = &pop_value;

void setUp(void) {
    queue__init(queue_inst_ptr);
    pop_value = 0;
}

void tearDown(void) {
}

void test_queue__init(void){
    TEST_ASSERT_EQUAL(0, queue_inst.queue_memory[0]);
    TEST_ASSERT_EQUAL(0, queue_inst.queue_memory[sizeof(queue_inst.queue_memory)-1]);
    TEST_ASSERT_EQUAL(0, queue_inst.queue_beginning);
    TEST_ASSERT_EQUAL(0, queue_inst.queue_end);
    TEST_ASSERT_TRUE(queue_inst.queue_is_initialized);
}

void test_queue__push(void){
    // Push an item to the first piece of the queue
    TEST_ASSERT_TRUE(queue__push(queue_inst_ptr, 5));
    TEST_ASSERT_EQUAL(5, queue_inst.queue_memory[queue_inst.queue_beginning]);
    TEST_ASSERT_EQUAL(1, queue_inst.queue_end);
    TEST_ASSERT_EQUAL(1, queue_inst.queue_num_items);

    // Fill the queue
    for (uint8_t mem_index = 0; mem_index < (sizeof(queue_inst.queue_memory))-1; mem_index++){
        TEST_ASSERT_TRUE(queue__push(queue_inst_ptr, 5));
    }
    TEST_ASSERT_FALSE(queue__push(queue_inst_ptr, 5));
    TEST_ASSERT_EQUAL(5, queue_inst.queue_memory[sizeof(queue_inst.queue_memory)/2]);
    TEST_ASSERT_EQUAL(0, queue_inst.queue_end);
    TEST_ASSERT_EQUAL(sizeof(queue_inst.queue_memory), queue_inst.queue_num_items);

    // De-init the queue and check failure
    queue_inst.queue_is_initialized = false;
    TEST_ASSERT_FALSE(queue__push(queue_inst_ptr, 5));
}

void test_queue__pop(void){
    // Push and Pop one item
    queue__push(queue_inst_ptr, 5);
    TEST_ASSERT_TRUE(queue__pop(queue_inst_ptr, pop_ptr));
    TEST_ASSERT_EQUAL(5, pop_value);
    TEST_ASSERT_EQUAL(1, queue_inst.queue_beginning);
    TEST_ASSERT_EQUAL(0, queue_inst.queue_num_items);

    // Fill and empty the queue
    for (uint8_t mem_index = 0; mem_index < (sizeof(queue_inst.queue_memory))-1; mem_index++){
        queue__push(queue_inst_ptr, mem_index);
        TEST_ASSERT_TRUE(queue__pop(queue_inst_ptr, pop_ptr));
    }
    TEST_ASSERT_FALSE(queue__pop(queue_inst_ptr, pop_ptr));
    TEST_ASSERT_EQUAL(0, queue_inst.queue_beginning);
    TEST_ASSERT_EQUAL(0, queue_inst.queue_num_items);
    TEST_ASSERT_EQUAL(sizeof(queue_inst.queue_memory)-2, pop_value);

    // De-init the queue and check failure
    queue_inst.queue_is_initialized = false;
    TEST_ASSERT_FALSE(queue__pop(queue_inst_ptr, pop_ptr));
}

void test_queue__get_count(void){

    // Check that it starts empty
    TEST_ASSERT_EQUAL(0, queue__get_count(queue_inst_ptr));

    // Add ten items then check that the count is 10
    for (uint8_t mem_index = 0; mem_index < 10; mem_index++){
        queue__push(queue_inst_ptr, 128);
    }
    TEST_ASSERT_EQUAL(10, queue__get_count(queue_inst_ptr));

    // Remove 5 items then check that the count is 5
    for (uint8_t mem_index = 0; mem_index < 5; mem_index++){
        queue__pop(queue_inst_ptr, pop_ptr);
    }
    TEST_ASSERT_EQUAL(5, queue__get_count(queue_inst_ptr));

    // De-init the queue and check 0
    queue_inst.queue_is_initialized = false;
    TEST_ASSERT_EQUAL(0, queue__get_count(queue_inst_ptr));
}
