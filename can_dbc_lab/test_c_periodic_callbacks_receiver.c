#include "unity.h"
#include "c_periodic_callbacks_receiver.h"
#include "MockLED_Display_wrapper.h"
#include "Mockc_gpio.h"
#include "Mockcan.h"
#include "_can_dbc/generated_can.h"


extern can_msg_t* rx_can_msg_ptr;
extern LIGHT_DATA_t light_msg_data;

void setUp(void) {

}

void tearDown(void) {
}

void test_c_period_init(void) {
    LED_wrap_init_ExpectAndReturn(true);
    CAN_init_ExpectAndReturn(can1, 100, 10, 10, NULL, NULL, true);
    CAN_reset_bus_Expect(can1);
    CAN_bypass_filter_accept_all_msgs_Expect();
    TEST_ASSERT_TRUE(c_period_init());
}

void test_c_period_reg_tlm(void){
    // Empty function
    TEST_ASSERT_TRUE(c_period_reg_tlm());
}

void test_c_period_1Hz(void){
    CAN_is_bus_off_ExpectAndReturn(can1, true);
    CAN_reset_bus_Expect(can1);
    c_period_1Hz(100);

    CAN_is_bus_off_ExpectAndReturn(can1, false);
    c_period_1Hz(100);
}

void test_c_period_10Hz(void){
    // Empty function
    c_period_10Hz(100);
}

void test_c_period_100Hz(void){
    CAN_rx_ExpectAndReturn(can1, rx_can_msg_ptr, 0, true);
    LED_wrap_setNumber_Expect(light_msg_data.LIGHT_DATA_light_percent);
    c_period_100Hz(100);
}

void test_c_period_1000Hz(void){
    // Empty function
    c_period_1000Hz(100);
}

