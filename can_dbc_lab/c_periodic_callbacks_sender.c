#include "c_periodic_callbacks_sender.h"
#include <stdio.h>
#include <stdbool.h>
#include "uart_wrapper.h"
#include "LED_Display_wrapper.h"
#include "can.h"
#include "c_gpio.h"
#include "LIGHT_Sensor_wrapper.h"
#include "_can_dbc/generated_can.h"

can_msg_t tx_can_message;
can_msg_t* tx_can_msg_ptr = &tx_can_message;
can_msg_t rx_can_message;
can_msg_t* rx_can_msg_ptr = &rx_can_message;


bool c_period_init(void)
{

    bool init_success = CAN_init(can1, 100, 10, 10, NULL, NULL);

    CAN_reset_bus(can1);
    CAN_bypass_filter_accept_all_msgs();


    return init_success;
}

bool c_period_reg_tlm(void)
{
    return true;
}

void c_period_1Hz(uint32_t count)
{
    if ((CAN_is_bus_off(can1)))
    {
        CAN_reset_bus(can1);
    }

    (void) count;
}
void c_period_10Hz(uint32_t count)
{

    LIGHT_DATA_t L = { 0 };
    L.LIGHT_DATA_light_percent = (uint8_t) LIGHT_wrap_getPercentValue(); //'1';

    can_msg_t can_msg = { 0 };

    // Encode the CAN message's data bytes, get its header and set the CAN message's DLC and length
    dbc_msg_hdr_t msg_hdr = dbc_encode_LIGHT_DATA(can_msg.data.bytes, &L);
    can_msg.msg_id = msg_hdr.mid;
    can_msg.frame_fields.data_len = msg_hdr.dlc;

    // Queue the CAN message to be sent out
    CAN_tx(can1, &can_msg, 0);

    (void) count;
}

void c_period_100Hz(uint32_t count)
{
    (void) count;
}
void c_period_1000Hz(uint32_t count)
{
    (void) count;
}
