/*
 * c_periodic_callbacks_receiver.c
 *
 *  Created on: Feb 16, 2019
 *      Author: reldn_000
 */

#include "c_periodic_callbacks_receiver.h"
#include "unit_test_facilitator.h"
#include <stdio.h>
#include <stdbool.h>
#include "uart_wrapper.h"
#include "LED_Display_wrapper.h"
#include "can.h"
#include "c_gpio.h"
#include "_can_dbc/generated_can.h"


can_msg_t rx_can_message;
can_msg_t* rx_can_msg_ptr = &rx_can_message;
LIGHT_DATA_t light_msg_data = { 0 };
//static uint8_t display_data = 0;

// The MIA functions require that you define the:
//   - Time when the handle_mia() functions will replace the data with the MIA
//   - The MIA data itself (ie: MOTOR_STATUS__MIA_MSG)
const uint32_t TEMPERATURE_DATA__MIA_MS = 3000;
const TEMPERATURE_DATA_t TEMPERATURE_DATA__MIA_MSG = { 0 };
const uint32_t LIGHT_DATA__MIA_MS = 3000;
const LIGHT_DATA_t LIGHT_DATA__MIA_MSG = {.LIGHT_DATA_light_percent = 95};

bool c_period_init(void)
{
    bool init_success = LED_wrap_init();

    CAN_init(can1, 100, 10, 10, NULL, NULL);

    CAN_reset_bus(can1);
    CAN_bypass_filter_accept_all_msgs();



    return init_success;
}

bool c_period_reg_tlm(void)
{
    return true;
}

void c_period_1Hz(uint32_t count)
{
    if ((CAN_is_bus_off(can1)))
    {
        CAN_reset_bus(can1);
    }

    (void) count;
}
void c_period_10Hz(uint32_t count)
{
    (void) count;
}

void c_period_100Hz(uint32_t count)
{


    if (CAN_rx(can1, rx_can_msg_ptr, 0))
    {
        //printf("Received message: %i\n", rx_can_message.data.bytes[0]);

        dbc_msg_hdr_t rx_msg_hdr;
        rx_msg_hdr.dlc = rx_can_message.frame_fields.data_len;
        rx_msg_hdr.mid = rx_can_message.msg_id;
        dbc_decode_LIGHT_DATA(&light_msg_data, rx_can_message.data.bytes, &rx_msg_hdr);

    }

    dbc_handle_mia_LIGHT_DATA(&light_msg_data, 10);
    LED_wrap_setNumber(light_msg_data.LIGHT_DATA_light_percent);

    (void) count;
}
void c_period_1000Hz(uint32_t count)
{
    (void) count;
}
