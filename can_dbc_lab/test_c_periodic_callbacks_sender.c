#include "unity.h"
#include "c_periodic_callbacks_sender.h"
#include "Mockc_gpio.h"
#include "Mockcan.h"
#include "MockLIGHT_Sensor_wrapper.h"
#include "_can_dbc/generated_can.h"



void setUp(void) {

}

void tearDown(void) {
}

void test_c_period_init(void) {
    CAN_init_ExpectAndReturn(can1, 100, 10, 10, NULL, NULL, true);
    CAN_reset_bus_Expect(can1);
    CAN_bypass_filter_accept_all_msgs_Expect();
    TEST_ASSERT_TRUE(c_period_init());
}

void test_c_period_reg_tlm(void){
    // Empty function
    TEST_ASSERT_TRUE(c_period_reg_tlm());
}

void test_c_period_1Hz(void){
    CAN_is_bus_off_ExpectAndReturn(can1, true);
    CAN_reset_bus_Expect(can1);
    c_period_1Hz(100);
}

void test_c_period_10Hz(void){
    CAN_tx_ExpectAndReturn(can1, NULL, 0, true);
    CAN_tx_IgnoreArg_msg();
    LIGHT_wrap_getPercentValue_ExpectAndReturn(98);
    c_period_10Hz(100);
}

void test_c_period_100Hz(void){
    // Empty function
    c_period_100Hz(100);
}

void test_c_period_1000Hz(void){
    // Empty function
    c_period_1000Hz(100);
}

